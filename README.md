# Get Started

This has been tested on Node v10.3.0 and NPM 6.1.0.

1. Run `npm install` to install required dependencies
2. Run `npm install -g gulp` to install gulp to the commandline
3. Run `npm start` to run the build. A server will be instantiated and the files
   will be served in your default browser.
