/* exported xhr */
var xhr = (function() {
    'use strict';

    var req;

    function request(url, cb) {
        req = new XMLHttpRequest();
        req.open('GET', url, true);
        req.onreadystatechange = function xhttpHandler() {
            if(this.readyState == 4 && this.status == 200) {
                cb(this.responseText);
            }
        }
        req.send();
    }

    return {
        request: request
    }
}());
