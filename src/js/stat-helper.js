/* exported statHelper */
function statHelper(d) {
    'use strict';

    var data = d;

    /* Data could be formatted more usefully */
    (function cleanup() {
        data.players.forEach(function(p) {
            /* Name stats in a useful way */
            p.stats = p.stats.reduce(function(accumulator, stat) {
                accumulator[stat.name] = stat.value;
                return accumulator;
            }, {});

            /* Convert codes to actual words */
            switch(p.player.info.position) {
                case 'D':
                    p.player.info.position = 'Defender'
                    break;
                case 'M':
                    p.player.info.position = 'Midfielder'
                    break;
                case 'F':
                    p.player.info.position = 'Forward'
                    break;
            }
        });
    })();

    /**
     * Return a player's full name and player ID in an object
     */
    function name(item) {
        return {
            id: item.player.id,
            name: item.player.name.first + ' ' + item.player.name.last
        }
    }

    /**
     * Match when player matches passed ID
     */
    function matchPlayer(item) {
        return item.player.id == this;
    }

    return {
        /**
         * Get Player Names
         */
        names: function() {
            return data.players.map(name);
        },

        /**
         * Get Player matching ID
         */
        player: function(id) {
            return data.players.filter(matchPlayer.bind(id))[0];
        },

        /**
         * Get name matching id
         */
        name: function(id) {
            return name(this.player(id));
        }
    }
}
