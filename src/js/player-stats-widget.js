(function() {
    'use strict';

    var data,
        playerPicker,
        option,
        elements = {
        },
        d;

    /**
     * Populates playerPicker with player data
     */
    function populateList() {
        data.names().forEach(function(item) {
            option = document.createElement('option');
            option.setAttribute('value', item.id);
            option.innerHTML = item.name;
            option.dataset.dynamic = '';
            playerPicker.appendChild(option);
        });
    }

    /**
     * Update Stats
     *
     * Writes player data to DOM
     */
    function updateStats() {
        if(!this) {
            return;
        }

        d = data.player(this.value);
        elements.name.innerHTML = data.name(this.value).name;
        elements.position.innerHTML = d.player.info.position;
        elements.image.setAttribute('src', './dist/images/p' + this.value + '.png');
        elements.image.setAttribute('alt', data.name(this.value).name);
        elements.team.dataset.team = d.player.currentTeam.shortName.replace(' ', '-').toLowerCase();
        elements.appearances.innerHTML = d.stats.appearances;
        elements.goals.innerHTML = d.stats.goals;
        elements.assists.innerHTML = d.stats.goal_assist || 0;
        elements.goalsPerMatch.innerHTML = (d.stats.goals / d.stats.appearances).toPrecision(1);
        elements.passesPerMinute.innerHTML = ((d.stats.fwd_pass + d.stats.backward_pass) / d.stats.mins_played).toPrecision(2);
    }

    function init() {
        playerPicker = document.querySelector('.player-picker');
        playerPicker.addEventListener('change', updateStats, false);

        // Player details
        elements.name = document.querySelector('[data-player-name]');
        elements.position = document.querySelector('[data-player-position]');
        elements.image = document.querySelector('[data-player-image]');
        elements.team = document.querySelector('.team-logo');

        // Stats
        elements.appearances = document.querySelector('[data-stat-appearances]');
        elements.goals = document.querySelector('[data-stat-goals]');
        elements.assists = document.querySelector('[data-stat-assists]');
        elements.goalsPerMatch = document.querySelector('[data-stat-goals-per-match]');
        elements.passesPerMinute = document.querySelector('[data-stat-passes-per-minute]');

        xhr.request('./player-stats.json', function(response) {
            data = new statHelper(JSON.parse(response));
            populateList();

            // Set default data
            updateStats.bind(playerPicker.querySelector('option[data-dynamic]'))();
        });
    }

    if(document.readyState == 'interactive') {
        init();
    } else {
        document.addEventListener('DOMContentLoaded', init, false);
    }
}());
