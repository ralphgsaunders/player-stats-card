'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    changed = require('gulp-changed'),
    imagemin = require('gulp-imagemin'),
    args = require('yargs').argv,
    autoprefixer = require('gulp-autoprefixer'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    browsersync = require('browser-sync').create(),
    packageName = 'player-stats-card',
    paths = {
        sass: {
            src: 'src/sass/**/*.scss',
            dist: 'dist/styles'
        },
        js: {
            src: [
                'src/js/xhr.js',
                'src/js/**/*.js'
            ],
            dist: 'dist/js'
        },
        images: {
            src: 'src/images/**/*',
            dist: 'dist/images'
        },
        packageWhitelist: [
            '*.html',
            '*.json',
            'dist/**/*'
        ]
    };

/**
 * `gulp sass`
 *
 * Compiles SCSS -> CSS.
 *
 * Also prefixes css properties for legacy browsers, as defined in the
 * autoprefixer options object.
 */
gulp.task('sass', function() {
    return gulp.src(paths.sass.src)
        .pipe(gulpif(!args.production, sourcemaps.init()))
        .pipe(gulpif(args.production, sass({
            outputStyle: 'compressed'
        }).on('error', sass.logError)))
        .pipe(gulpif(!args.production, sass().on('error', sass.logError)))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            flexbox: 'no-2009',
            cascade: false
        }))
        .pipe(gulpif(!args.production, sourcemaps.write()))
        .pipe(gulp.dest(paths.sass.dist));
});

/**
 * `gulp js`
 *
 * Pipes changed source JS -> dist file named bundle.min.js
 *
 * By default all files are concat together and are sourcemapped.
 *
 * JSHint runs over all javascript, and is configured by the .jshintrc file in
 * the repo. Global variables will need to be explicitly declared in this file.
 * [Find out how to do that here](http://jshint.com/docs/options/#globals).
 *
 * JSHint can also be configured to ignore files in the same way .gitignore
 * works, in the .jshintignore file.
 *
 * When running tasks with the --production flag, sourcemaps are removed and the
 * bundle.min.js file is compressed.
 */
gulp.task('js', function() {
    return gulp.src(paths.js.src)
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(gulpif(!args.production, sourcemaps.init()))
        .pipe(concat('bundle.min.js'))
        .pipe(gulpif(args.production, uglify({
            mangle: false
        })))
        .pipe(gulpif(!args.production, sourcemaps.write()))
        .pipe(gulp.dest(paths.js.dist));
});

/**
 * `gulp images`
 *
 * Pipes changed/new images.
 * Optimises files for filesize, including SVGs.
 */
gulp.task('images', function() {
    return gulp.src(paths.images.src)
        .pipe(changed(paths.images.dist))
        .pipe(imagemin([
            imagemin.gifsicle(),
            imagemin.jpegtran(),
            imagemin.optipng(),
            imagemin.svgo({plugins: [
                {mergePaths: false},
                {removeAttrs: false},
                {convertShapeToPath: false},
                {sortAttrs: true}
            ]})
        ]))
        .pipe(gulp.dest(paths.images.dist));
});

/**
 * `gulp package`
 *
 * Process all the assets and send to the package folder
 */
gulp.task('package', ['sass', 'js', 'images'], function(){
    return gulp.src(paths.packageWhitelist, { base: './' })
      .pipe(gulp.dest('../' + packageName + '-package/'));
});

gulp.task('default', ['sass', 'js', 'images'], function() {
    browsersync.init({
        server: './',
        https: false,
    });

    gulp.watch(paths.sass.src, ['sass']);
    gulp.watch(paths.js.src, ['js']);
    gulp.watch(paths.images.src, ['images']);
    gulp.watch(['*.json', '*.html', paths.js.src]).on('change', browsersync.reload);
});

